var __defProp = Object.defineProperty;
var __defProps = Object.defineProperties;
var __getOwnPropDescs = Object.getOwnPropertyDescriptors;
var __getOwnPropSymbols = Object.getOwnPropertySymbols;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __propIsEnum = Object.prototype.propertyIsEnumerable;
var __defNormalProp = (obj, key, value) => key in obj ? __defProp(obj, key, { enumerable: true, configurable: true, writable: true, value }) : obj[key] = value;
var __spreadValues = (a, b) => {
  for (var prop in b || (b = {}))
    if (__hasOwnProp.call(b, prop))
      __defNormalProp(a, prop, b[prop]);
  if (__getOwnPropSymbols)
    for (var prop of __getOwnPropSymbols(b)) {
      if (__propIsEnum.call(b, prop))
        __defNormalProp(a, prop, b[prop]);
    }
  return a;
};
var __spreadProps = (a, b) => __defProps(a, __getOwnPropDescs(b));
(function(global, factory) {
  typeof exports === "object" && typeof module !== "undefined" ? factory(require("@karsegard/react-web-component"), require("react"), require("prop-types")) : typeof define === "function" && define.amd ? define(["@karsegard/react-web-component", "react", "prop-types"], factory) : (global = typeof globalThis !== "undefined" ? globalThis : global || self, factory(global.reactWebComponent, global.React, global.PropTypes));
})(this, function(reactWebComponent, React, PropTypes) {
  "use strict";
  function _interopDefaultLegacy(e) {
    return e && typeof e === "object" && "default" in e ? e : { "default": e };
  }
  var React__default = /* @__PURE__ */ _interopDefaultLegacy(React);
  var PropTypes__default = /* @__PURE__ */ _interopDefaultLegacy(PropTypes);
  var select = "";
  function deepMap(children, condition, callback) {
    return React.Children.map(children, (child) => {
      if (child === null)
        return null;
      if (condition && condition(child)) {
        return callback(child);
      }
      if (child.props && child.props.children && typeof child.props.children === "object") {
        return React.cloneElement(child, __spreadProps(__spreadValues({}, child.props), {
          children: deepMap(child.props.children, condition, callback)
        }));
      }
      return child;
    });
  }
  const Select = (props) => {
    const [isOptionsOpen, setIsOptionsOpen] = React.useState(false);
    const [loaded, setLoaded] = React.useState(false);
    const [selectedOption, setSelectedOption] = React.useState(0);
    const [selectedValue, setSelectedValue] = React.useState("");
    const [suffix, setSuffix] = React.useState("");
    const [prefix, setPrefix] = React.useState("");
    const [optionsList, setOptions] = React.useState([]);
    const ref = React.useRef();
    const { name, children, value, onChange, placeholder } = props;
    const toggleOptions = () => {
      setIsOptionsOpen(!isOptionsOpen);
    };
    const setSelectedThenCloseDropdown = (index) => {
      setSelectedOption(index);
      setIsOptionsOpen(false);
    };
    const handleKeyDown = (index) => (e) => {
      switch (e.key) {
        case " ":
        case "SpaceBar":
        case "Enter":
          e.preventDefault();
          setSelectedThenCloseDropdown(index);
          break;
      }
    };
    const handleListKeyDown = (e) => {
      switch (e.key) {
        case "Escape":
          e.preventDefault();
          setIsOptionsOpen(false);
          break;
        case "ArrowUp":
          e.preventDefault();
          setSelectedOption(selectedOption - 1 >= 0 ? selectedOption - 1 : optionsList.length - 1);
          break;
        case "ArrowDown":
          e.preventDefault();
          setSelectedOption(selectedOption == optionsList.length - 1 ? 0 : selectedOption + 1);
          break;
      }
    };
    React.useEffect(() => {
      let options = [];
      let _suffix = "";
      let _prefix = "";
      setLoaded(false);
      deepMap(children, (element) => element.type && element.type == "option", (node) => {
        if (node.props && node.props.value) {
          options.push({ value: node.props.value, text: node.props.children });
        } else if (node.props && node.props.children) {
          options.push({ value: node.props.children, text: node.props.children });
        }
      });
      deepMap(children, (element) => element.type && element.type != "option" && element.props["data-role"] == "after", (node) => {
        _suffix = node.props.children;
      });
      deepMap(children, (element) => element.type && element.type != "option" && element.props["data-role"] == "before", (node) => {
        _prefix = node.props.children;
      });
      setOptions(options);
      setSuffix(_suffix);
      setPrefix(_prefix);
      setLoaded(true);
    }, [children]);
    const handleClick = (event) => {
      if (ref.current && !ref.current.contains(event.target)) {
        setIsOptionsOpen(false);
      }
    };
    React.useEffect(() => {
      document.addEventListener("click", handleClick, true);
      return () => {
        document.removeEventListener("click", handleClick, true);
      };
    }, [ref]);
    React.useEffect(() => {
      if (optionsList[selectedOption]) {
        let value2 = optionsList[selectedOption].value;
        setSelectedValue(value2);
      }
    }, [selectedOption, optionsList]);
    React.useEffect(() => {
      if (optionsList.length > 0) {
        onChange({ selectedValue, selectedOption });
      }
    }, [selectedValue]);
    React.useEffect(() => {
      if (value && !selectedValue && optionsList) {
        let selected = optionsList.reduce((carry, item, index) => {
          if (item.value == value) {
            carry = index;
          }
          return carry;
        }, null);
        setSelectedOption(selected);
      }
    }, [optionsList]);
    let className = "k-select wrapper " + (props.className ? props.className : "") + " " + (loaded ? "loaded" : "loading");
    return /* @__PURE__ */ React__default["default"].createElement("div", {
      className,
      ref
    }, /* @__PURE__ */ React__default["default"].createElement("input", {
      type: "hidden",
      name,
      value: selectedValue
    }), /* @__PURE__ */ React__default["default"].createElement("div", {
      className: "container"
    }, /* @__PURE__ */ React__default["default"].createElement("button", {
      type: "button",
      "aria-haspopup": "listbox",
      "aria-expanded": isOptionsOpen,
      className: isOptionsOpen ? "expanded" : "",
      onClick: toggleOptions,
      onKeyDown: handleListKeyDown
    }, prefix, "  ", /* @__PURE__ */ React__default["default"].createElement("span", {
      className: "value"
    }, optionsList[selectedOption] && optionsList[selectedOption].text || placeholder), " ", suffix), /* @__PURE__ */ React__default["default"].createElement("ul", {
      className: `options ${isOptionsOpen ? "show" : ""}`,
      role: "listbox",
      tabIndex: -1,
      onKeyDown: handleListKeyDown
    }, optionsList.map((option, index) => /* @__PURE__ */ React__default["default"].createElement("li", {
      id: option.value,
      role: "option",
      key: index,
      "aria-selected": selectedOption == index,
      tabIndex: 0,
      onKeyDown: handleKeyDown(index),
      onClick: () => {
        setSelectedThenCloseDropdown(index);
      }
    }, option.text)))));
  };
  Select.propTypes = {
    options: PropTypes__default["default"].object,
    onChange: PropTypes__default["default"].func,
    value: PropTypes__default["default"].string,
    name: PropTypes__default["default"].string,
    placeholder: PropTypes__default["default"].string
  };
  customElements.define("k-select", reactWebComponent.ReactWebComponent(Select));
});
