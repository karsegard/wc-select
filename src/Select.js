import React, { useState, useEffect, useRef, Children, cloneElement } from 'react';
import '@/scss/select.scss'
import PropTypes from 'prop-types';




function makeTypeChecker(type) {
    return (element) => !!element.type && element.type === type;
}

export const isOption = makeTypeChecker('option');


export function deepMap(children, condition, callback) {
    return Children.map(children, (child) => {

        if (child === null) return null;
        if (condition && condition(child)) {
            return callback(child);
        }
        if (
            child.props &&
            child.props.children &&
            typeof child.props.children === 'object'
        ) {
            // Clone the child that has children and map them too
            return cloneElement(child, {
                ...child.props,
                children: deepMap(child.props.children,condition, callback),
            });
        }

        return child;
    });
}



export const Select = (props) => {

    const [isOptionsOpen, setIsOptionsOpen] = useState(false);
    const [loaded, setLoaded] = useState(false);
    const [selectedOption, setSelectedOption] = useState(0);
    const [selectedValue, setSelectedValue] = useState('');
    const [suffix, setSuffix] = useState('');
    const [prefix, setPrefix] = useState('');
    const [optionsList, setOptions] = useState([]);
    const ref = useRef();


    const { name, children, value, onChange,placeholder } = props;
    const toggleOptions = () => {
        setIsOptionsOpen(!isOptionsOpen);
    };

    const setSelectedThenCloseDropdown = (index) => {
        setSelectedOption(index);
        setIsOptionsOpen(false);

    };

    const handleKeyDown = (index) => (e) => {
        switch (e.key) {
            case " ":
            case "SpaceBar":
            case "Enter":
                e.preventDefault();
                setSelectedThenCloseDropdown(index);
                break;
            default:
                break;
        }
    };

    const handleListKeyDown = (e) => {
        switch (e.key) {
            case "Escape":
                e.preventDefault();
                setIsOptionsOpen(false);
                break;
            case "ArrowUp":
                e.preventDefault();
                setSelectedOption(
                    selectedOption - 1 >= 0 ? selectedOption - 1 : optionsList.length - 1
                );
                break;
            case "ArrowDown":
                e.preventDefault();
                setSelectedOption(
                    selectedOption == optionsList.length - 1 ? 0 : selectedOption + 1
                );
                break;
            default:
                break;
        }
    };

    useEffect(() => {
        let options = [];
        let _suffix='';
        let _prefix='';
        setLoaded(false);
        deepMap(children, element=> (element.type && element.type =="option"), node => {
            if (node.props && node.props.value) {
                options.push({ value: node.props.value, text: node.props.children });
            } else if(node.props && node.props.children){
                options.push({ value: node.props.children, text: node.props.children });

            }
        });
        
        deepMap(children, element=> (element.type && element.type !="option" && element.props['data-role']=='after'), node => {
           _suffix = node.props.children;
        });
        deepMap(children, element=> (element.type && element.type !="option" && element.props['data-role']=='before'), node => {
            _prefix = node.props.children;
         });
        setOptions(options);
        setSuffix(_suffix);
        setPrefix(_prefix);
        setLoaded(true);

    }, [children]);



    const handleClick = event => {
        if (ref.current && !ref.current.contains(event.target)) {
            setIsOptionsOpen(false);

        }
    }

    useEffect(() => {

        document.addEventListener('click', handleClick, true);
        return () => {
            document.removeEventListener('click', handleClick, true);
        };

    }, [ref])

    useEffect(() => {
        if (optionsList[selectedOption]) {
            let value = optionsList[selectedOption].value;
            setSelectedValue(value);

        }
    }, [selectedOption, optionsList]);

    useEffect(() => {
        if (optionsList.length > 0) {
            onChange({ selectedValue, selectedOption });
        }
    }, [selectedValue]);

    useEffect(() => {
        if (value && !selectedValue && optionsList) {
            let selected = optionsList.reduce((carry, item, index) => {
                if (item.value == value) {
                    carry = index;
                }
                return carry;
            }, null);
            setSelectedOption(selected);
        }
    }, [optionsList]);

    let className = "k-select wrapper " + (props.className ? props.className : '') +  ' '+(loaded?'loaded':'loading') ;
    return (
        <div className={className} ref={ref}>
            <input type="hidden" name={name} value={selectedValue} />
            <div className="container">
                <button
                    type="button"
                    aria-haspopup="listbox"
                    aria-expanded={isOptionsOpen}
                    className={isOptionsOpen ? "expanded" : ""}
                    onClick={toggleOptions}
                    onKeyDown={handleListKeyDown}
                >
                  {prefix}  <span className="value">{optionsList[selectedOption] && optionsList[selectedOption].text || placeholder}</span> {suffix}
                    
                </button>
                <ul
                    className={`options ${isOptionsOpen ? "show" : ""}`}
                    role="listbox"

                    tabIndex={-1}
                    onKeyDown={handleListKeyDown}
                >
                    {optionsList.map((option, index) => (
                        <li
                            id={option.value}
                            role="option"
                            key={index}
                            aria-selected={selectedOption == index}
                            tabIndex={0}
                            onKeyDown={handleKeyDown(index)}
                            onClick={() => {
                                setSelectedThenCloseDropdown(index);
                            }}
                        >
                            {option.text}
                        </li>
                    ))}
                </ul>
            </div>
        </div>
    );
}


Select.propTypes = {
    options: PropTypes.object,
    onChange: PropTypes.func,
    value: PropTypes.string,
    name: PropTypes.string,
    placeholder: PropTypes.string
};

export default Select;